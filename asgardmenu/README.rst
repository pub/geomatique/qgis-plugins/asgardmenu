AsgardMenu
==========

Extension QGIS pour la découverte du patrimoine de données en base PostgreSQL
sous la forme d'un menu, développée dans le cadre des travaux du groupe de
travail interministériel PostGIS (MAA, MTE/MCTRCT/Mer).

Dépôt
-----

AsgardMenu est disponible sur le `dépôt des extensions QGIS MAA/MTE/MCTRCT/Mer`__.

.. __ : http://piece-jointe-carto.developpement-durable.gouv.fr/NAT002/QGIS/plugins/plugins.xml

Documentation
-------------

https://snum.scenari-community.org/Asgard/Documentation/

Crédits
=======

Copyright République Française, 2020. Secrétariat général du Ministère de la transition
écologique, du Ministère de la cohésion des territoires et des relations avec les
collectivités territoriales et du Ministère de la Mer.
Service du numérique.

Contributeur : Alain Ferraton (SNUM/DS/GSG).

AsgardMenu est dérivé de l'extension QGIS MenuBuilder, développée par la société Oslandia_
et financée par le Ministère de l'écologie, du développement durable et de l'énergie (DREAL
Rhône-Alpes). MenuBuilder est publié sous licence GPL-2.0 ou postérieure sur le `dépôt des
extensions QGIS`_ et le GitLab_ de la société Oslandia.

.. _Oslandia : http://www.oslandia.com
.. _dépôt des extensions QGIS : http://plugins.qgis.org/plugins/MenuBuilder/
.. _GitLab : https://gitlab.com/Oslandia/qgis/qgis-menu-builder


License
=======

AsgardMenu est un logiciel libre publié sous licence `GNU General Public Licence v3.0 seule`__ (GPL-3.0-only).
Cf. fichier "license".

.. __ : https://spdx.org/licenses/GPL-3.0.html