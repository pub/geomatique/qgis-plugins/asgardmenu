# -*- coding: utf-8 -*-
"""
AsgardMenu - 



/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 
"""
import os
import re #pour les expressions rationelles
import json
from contextlib import contextmanager
from collections import defaultdict
from functools import wraps, partial

import psycopg2

from PyQt5.QtCore import Qt, QRect, QSortFilterProxyModel, QFile, QDir
from PyQt5.QtWidgets import (
    QAction, QMessageBox, QDialog, QMenu, QTreeView,
    QAbstractItemView, QDockWidget, QWidget, QVBoxLayout,
    QSizePolicy, QLineEdit, QDialogButtonBox, QTableWidgetItem, QCheckBox, QHBoxLayout
)

from PyQt5.QtGui import (
    QIcon, QStandardItem,
    QStandardItemModel
)
from qgis.core import (
    QgsProject, QgsBrowserModel, QgsDataSourceUri, QgsSettings,
    QgsCredentials, QgsVectorLayer, QgsMimeDataUtils, QgsRasterLayer, QgsProviderRegistry
)

from .asgard_menu_dialog_base import Ui_Dialog
from .bibli_sql import dicListSql

QGIS_MIMETYPE = 'application/x-vnd.qgis.qgis.uri'


ICON_MAPPER = {
    'postgres': ":/plugins/AsgardMenu/resources/postgis.svg",
    'WMS': ":/plugins/AsgardMenu/resources/wms.svg",
    'WFS': ":/plugins/AsgardMenu/resources/wfs.svg",
    'OWS': ":/plugins/AsgardMenu/resources/ows.svg",
    'spatialite': ":/plugins/AsgardMenu/resources/spatialite.svg",
    'mssql': ":/plugins/AsgardMenu/resources/mssql.svg",
    'gdal': ":/plugins/AsgardMenu/resources/gdal.svg",
    'ogr': ":/plugins/AsgardMenu/resources/ogr.svg",
    'POINT_autre' : ":/plugins/AsgardMenu/resources/mIconPointLayerLecteur.svg",
    'STRING_autre' : ":/plugins/AsgardMenu/resources/mIconLineLayerLecteur.svg",
    'POLYGON_autre' : ":/plugins/AsgardMenu/resources/mIconPolygonLayerLecteur.svg",
    'NoGeometry_autre' : ":/plugins/AsgardMenu/resources/mIconTableLayerLecteur.svg",
    'POINT_lecteur' : ":/plugins/AsgardMenu/resources/mIconPointLayerLecteur.svg",
    'STRING_lecteur' : ":/plugins/AsgardMenu/resources/mIconLineLayerLecteur.svg",
    'POLYGON_lecteur' : ":/plugins/AsgardMenu/resources/mIconPolygonLayerLecteur.svg",
    'NoGeometry_lecteur' : ":/plugins/AsgardMenu/resources/mIconTableLayerLecteur.svg",
    'POINT_producteur' : ":/plugins/AsgardMenu/resources/mIconPointLayerProducteur.svg",
    'STRING_producteur' : ":/plugins/AsgardMenu/resources/mIconLineLayerProducteur.svg",
    'POLYGON_producteur' : ":/plugins/AsgardMenu/resources/mIconPolygonLayerProducteur.svg",
    'NoGeometry_producteur' : ":/plugins/AsgardMenu/resources/mIconTableLayerProducteur.svg",
    'POINT_editeur' : ":/plugins/AsgardMenu/resources/mIconPointLayerEditeur.svg",
    'STRING_editeur' : ":/plugins/AsgardMenu/resources/mIconLineLayerEditeur.svg",
    'POLYGON_editeur' : ":/plugins/AsgardMenu/resources/mIconPolygonLayerEditeur.svg",
    'NoGeometry_editeur' : ":/plugins/AsgardMenu/resources/mIconTableLayerEditeur.svg",     
    'INDETERMINE_lecteur' : ":/plugins/AsgardMenu/resources/mIconUndefinedLecteur.svg",
    'INDETERMINE_editeur' : ":/plugins/AsgardMenu/resources/mIconUndefinedEditeur.svg",
    'INDETERMINE_producteur' : ":/plugins/AsgardMenu/resources/mIconUndefinedProducteur.svg",
    'INDETERMINE_autre' : ":/plugins/AsgardMenu/resources/mIconUndefinedLecteur.svg"    
}



class AsgardMenuDialog(QDialog, Ui_Dialog):

    def __init__(self, uiparent):
        super().__init__()

        self.setupUi(self)

        # reference to caller
        self.uiparent = uiparent

        # add a dock widget
        self.dock_widget = QDockWidget("Menus")
        self.dock_widget.resize(400, 300)
        self.dock_widget.setFloating(True)
        self.dock_widget.setObjectName(self.tr("Menu Tree"))
        self.dock_widget_content = QWidget()
        self.dock_widget.setWidget(self.dock_widget_content)
        dock_layout = QVBoxLayout()
        self.dock_widget_content.setLayout(dock_layout)
        self.dock_view = DockQtTreeView(self.dock_widget_content)
        self.dock_view.setDragDropMode(QAbstractItemView.DragOnly)
        self.dock_menu_filter = QLineEdit()
        self.dock_menu_filter.setPlaceholderText(self.tr("Filtrer avec les commentaires des couches"))
        dock_layout.addWidget(self.dock_menu_filter)
        dock_layout.addWidget(self.dock_view)
        self.dock_view.setHeaderHidden(True)
        self.dock_view.setDragEnabled(True)
        self.dock_view.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.dock_view.setAnimated(True)
        self.dock_view.setObjectName("treeView")
        self.proxy_model = LeafFilterProxyModel(self)
        self.proxy_model.setFilterRole(Qt.ToolTipRole)
        self.proxy_model.setFilterCaseSensitivity(Qt.CaseInsensitive)

        self.profile_list = []
        self.table = 'qgis_menubuilder_metadata'
        self.dict_connections = {} #dictionnaire des connexions {connection : chaine initiale}
        self_dict_alias = {} #dictionnaire des alias de connexions
        self.connections_actives = [] #liste des connexions actives
        self.showBlocks = False #niveau bloc (autorisé si une seule connection)
        self.splitByBlock = True #Afficher un menu par bloc dans le menu principal
        self.combine = False #regroupement des menus (si plusieurs connections)
        self.combineMenuName='Patrimoine' #nom du menu par défaut 
        #self.combineHideConnectionName = False #masquage des noms de connections (regroupement de menus)
        self.showConnection = True #Affichage du niveau connexion
        self.blockFirst= True #niveau bloc avant celui des connexions
        self.showRelationKind = False #affichage dy type de relation entre parenthèse dans le nom de l'objet
        self.layerNameFromComment = False #nom de la couche d'après une expression rationelle sur les commentaires
        self.layerRegexp = "[[]([^]]+)[]]" # blabla [nom court] blabla -> par défaut
        self.menubar = False #menu dans le menu principal de QGIS
        self.dock = False #menu dans le dockwidget
        #autres expressions possible  
        # Nom court. blabla -> "^([^.]+)[.]"
        # blabla.Nom court -> "[.]([^.]+)[.]?$"

        self.layer_handler = {
            'vector': self.load_vector,
            'raster': self.load_raster
        }
        
        # connect signals and handlers
        self.dock_menu_filter.textEdited.connect(self.filter_update)
        self.dock_view.doubleClicked.connect(self.load_from_index)
        self.checkBox.stateChanged.connect(self.levelBloc)
        self.checkBox_2.stateChanged.connect(self.combineMenu)
        self.checkShowConnections.stateChanged.connect(self.showConnectionParam)
        self.checkBlocFirst.stateChanged.connect(self.blocFirstParam)
        self.checkBox_4.stateChanged.connect(self.showRelation)
        self.checkBox_5.stateChanged.connect(self.nameFromComment)
        self.activate_menubar.stateChanged.connect(self.menubar_changed)
        self.activate_dock.stateChanged.connect(self.dock_changed)

        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.apply)
        self.table_database.itemClicked.connect(self.handleItemClicked)

    def blocFirstParam(self, state) :
        if state == Qt.Checked :
            self.blockFirst= True
        else :
            self.blockFirst = False 
  

    def nameFromComment(self, state) :
        if state == Qt.Checked :
            self.layerNameFromComment = True
        else :
            self.layerNameFromComment = False        

    def showRelation(self, state) :
        if state == Qt.Checked :
            self.showRelationKind = True
        else :
            self.showRelationKind = False        

    def showConnectionParam(self, state) :
        if state == Qt.Checked :
            self.showConnection = True
            if self.showBlocks : self.checkBlocFirst.setDisabled(False)
        else :
            self.showConnection = False
            self.checkBlocFirst.setDisabled(True)
            if len(self.connections_actives) <= 1 and self.showBlocks == False:
                QMessageBox.critical(self, "Error", "la connexion doit être active si bloc n'est pas actif")
                self.showConnection = True
                self.checkShowConnections.setCheckState(Qt.Checked)

    def combineMenu(self, state):
        if state == Qt.Checked :
            self.combine = True
        else :
            self.combine = False

    def levelBloc(self, state):
        if state == Qt.Checked :
            self.showBlocks = True
            if self.showConnection : self.checkBlocFirst.setDisabled(False)
        else :
            self.showBlocks = False
            self.checkBlocFirst.setDisabled(True)
            #le niveau 'connexion' est forcement actif si on n'affiche pas les blocs dans le cas d'une seule connexion
            if len(self.connections_actives) <= 1 : 
                self.showConnection = True
                #self.checkShowConnections.setDisabled(True)
                self.checkShowConnections.setCheckState(Qt.Checked)
                
    def menubar_changed(self, state) :
        if state == Qt.Checked :
            self.menubar = True
            if len(self.connections_actives) > 1 : self.groupBoxMenu.setDisabled(False) #regroupement actif
        else :
            self.menubar = False 
            #Regroupement inactif
            self.groupBoxMenu.setDisabled(True) 
            self.checkBox_2.setCheckState(Qt.Unchecked)
            self.combine = False

    def dock_changed(self, state) :
        if state == Qt.Checked :
            self.dock = True
        else :
            self.dock = False
    
    def handleItemClicked(self, item):
        connection = self.table_database.item(item.row(),0).text()
        if (self.table_database.item(item.row(),1).checkState()) == 2 :
            if self.set_connection(connection):
                self.connections_actives.append(connection)
            else :
                self.table_database.item(item.row(),1).setCheckState(Qt.Unchecked)
        else :
            if connection in self.connections_actives : self.connections_actives.remove(connection)

        #on ne peut regrouper les menus que pour plusieurs connexions:
        if len(self.connections_actives) > 1 : 
            if self.menubar : self.groupBoxMenu.setDisabled(False)
            self.checkShowConnections.setDisabled(False) #on réactive showConnection si necesaire
        else :
            self.groupBoxMenu.setDisabled(True)
            self.checkBox_2.setCheckState(Qt.Unchecked)
            self.combine = False
            if self.showBlocks == True :
                if self.blockFirst == False : #si les blocs ne sont pas en premier 
                    self.showConnection = True
                    self.checkShowConnections.setCheckState(Qt.Checked)
            else : 
                QMessageBox.critical(self, "Error", "la connexion doit être active si bloc n'est pas actif")
                self.showConnection = True
                self.checkShowConnections.setCheckState(Qt.Checked)            
        
    def filter_update(self):
        text = self.dock_menu_filter.displayText()
        self.proxy_model.setFilterRegExp(text)

    def show_dock(self, state):
        if not state:
            # just hide widget
            self.dock_widget.setVisible(state)
            return   
        # dock must be read only and deepcopy of model is not supported (c++ inside!)
        self.dock_model = MenuTreeModel(self)
        self.build_dock(self.dock_model)

        self.dock_model.setHorizontalHeaderLabels(["Menus"])
        self.dock_view.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.proxy_model.setSourceModel(self.dock_model)
        self.dock_view.setModel(self.proxy_model)
        self.uiparent.iface.addDockWidget(Qt.LeftDockWidgetArea, self.dock_widget)
        self.dock_widget.setVisible(state)

    def show_menus(self, state):
        if state:
            self.build_menu()
            return
        # remove menus
        for menu in self.uiparent.menus:
            self.uiparent.iface.mainWindow().menuBar().removeAction(menu.menuAction())


    def update_database_list(self):
        """update list of defined postgres connections"""
        settings = QgsSettings()
        settings.beginGroup("/PostgreSQL/connections")
        keys = settings.childGroups()
        self.table_database.clear()
        self.table_database.setRowCount(len(keys))

        for i,key in enumerate(keys) :       
            self.table_database.setItem(i, 0, QTableWidgetItem(key))

            chkBoxItem = QTableWidgetItem()
            chkBoxItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
            if key in self.connections_actives :
                chkBoxItem.setCheckState(Qt.Checked)
            else :
                chkBoxItem.setCheckState(Qt.Unchecked)
            self.table_database.setItem(i, 1, chkBoxItem)
            if key in self.dict_alias :
                self.table_database.setItem(i, 2, QTableWidgetItem(self.dict_alias[key]))

        #self.resize(int(self.dialogWidth), int(self.dialogHeight))
        #self.table_database.resizeColumnsToContents()
        self.table_database.setHorizontalHeaderItem(0,QTableWidgetItem("connection"))
        self.table_database.setHorizontalHeaderItem(1,QTableWidgetItem("Actif/inactif"))
        self.table_database.setHorizontalHeaderItem(2,QTableWidgetItem("Alias"))
        #self.table_database.setColumnWidth(0,self.column0Width)
        #self.table_database.setColumnWidth(1,self.column1Width)
        #self.table_database.setColumnWidth(2,self.column2Width)        

        settings.endGroup()

        if self.dock : 
            self.activate_dock.setCheckState(Qt.Checked)
        else :
            self.activate_dock.setCheckState(Qt.Unchecked)
        if self.menubar : 
            self.activate_menubar.setCheckState(Qt.Checked)
        else :
            self.activate_menubar.setCheckState(Qt.Unchecked)
        
        #on ne peut regrouper les menus que pour plusieurs connexions et pour si menus est actif:
        if len(self.connections_actives) > 1 and self.menubar: 
            self.groupBoxMenu.setDisabled(False)
            if self.combine : self.checkBox_2.setCheckState(Qt.Checked)
        else :
            self.groupBoxMenu.setDisabled(True)
            self.checkBox_2.setCheckState(Qt.Unchecked)
            self.combine = False
        
        if self.showConnection :
            self.checkShowConnections.setCheckState(Qt.Checked)
        else :
            self.checkShowConnections.setCheckState(Qt.Unchecked)

        if self.blockFirst :
            self.checkBlocFirst.setCheckState(Qt.Checked)
        else :
            self.checkBlocFirst.setCheckState(Qt.Unchecked)
            
        if self.showBlocks :
            self.checkBox.setCheckState(Qt.Checked)
        else :
            self.checkBox.setCheckState(Qt.Unchecked)

        if self.layerNameFromComment : 
           self.checkBox_5.setCheckState(Qt.Checked)
        else :
           self.checkBox_5.setCheckState(Qt.Unchecked)
        
        if self.showRelationKind : 
            self.checkBox_4.setCheckState(Qt.Checked)
        else :
            self.checkBox_4.setCheckState(Qt.Unchecked)
        
        self.lineEdit.setText(self.combineMenuName)
        self.lineEdit_2.setText(self.layerRegexp)

    def set_connection(self, dbname=None):

        metadata = QgsProviderRegistry.instance().providerMetadata('postgres')

        if dbname not in metadata.connections(False) :
           QMessageBox.critical(self, "Error", "new : There is no defined database connection for " + dbname)
           selected = None
        else:
           selected=dbname
        #Connect to dbname postgresql database
                        

         
        if not selected:
            return False
 
        
        #modification pour permettre d'utiliser les configurations du passwordManager (QGIS >=3.10)
        metadata = QgsProviderRegistry.instance().providerMetadata('postgres')
        if dbname not in metadata.connections(False) :
                QMessageBox.critical(self, "Error", "new : There is no defined database connection for " + selected)

        
        if selected not in self.dict_connections : #si c'est la première fois que l'on utilise la connexion dans la session
            #self.dict_connections[selected]= uri.connectionInfo() - modifié pour utiliser metadata
            self.dict_connections[selected]= metadata.findConnection(selected).uri()
        
        #récupération à l'ancienne de l'URI pour ne pas faire trop de modif dans le code
        uri = QgsDataSourceUri(metadata.findConnection(selected).uri())

        # connect to db
        try:
            self.connect_to_uri(uri)
        except self.pg_error_types():
            QMessageBox.warning(
                self,
                "Plugin AsgardMenu: Message",
                self.tr("The database is unavailable"),
                QMessageBox.Ok,
            )
            # connection not available
            return False

        return self.verify_asgard_extension(dbname)
        #return True

    @contextmanager
    def transaction(self):
        try:
            yield
            self.connection.commit()
        except self.pg_error_types() as e:
            self.connection.rollback()
            raise e

    def connect_to_uri(self, uri):
        self.close_connection()
        self.host = uri.host() or os.environ.get('PGHOST')
        self.port = uri.port() or os.environ.get('PGPORT')

        username = uri.username() or os.environ.get('PGUSER') or os.environ.get('USER')
        password = uri.password() or os.environ.get('PGPASSWORD')


        conninfo = uri.connectionInfo()


        while True:
            try:
                self.connection = psycopg2.connect(uri.connectionInfo(), application_name="QGIS:AsgardMenu")
                break
            except self.pg_error_types() as e:
                err = str(e) or "Erreur d'authentification. Vérifiez les informations saisies."
                                
                ok, username, password = QgsCredentials.instance().get(
                    conninfo, username, password, err)
                if not ok:
                    raise e
                if username:
                    uri.setUsername(username)
                if password:
                    uri.setPassword(password)
        
        QgsCredentials.instance().put(conninfo, username, password) 
        self.pgencoding = self.connection.encoding

        return True

    def pg_error_types(self):
        return (
            psycopg2.InterfaceError,
            psycopg2.OperationalError,
            psycopg2.ProgrammingError
        )

    def verify_asgard_extension(self,dbname):
    
        # A partir de AsgardMenu V0.2 on vérifie également que postgis est installé.
        # A partir de AsgardMenu V1.6 on vérifie que le rôle de connexion à les droits sur la vue de gestion d'asgard
        verif_postgis = False
        verif_asgard = False
        verif_privilege = False
        

        with self.transaction():
            cur = self.connection.cursor()
            cur.execute("""
                select extname, extversion from pg_extension
                """)                
            row=cur.fetchone()           
            
            while row is not None:
                if row[0] == 'asgard' and row[1] >= '1.1.0' :
                    verif_asgard = True
                if row[0] == 'postgis' :
                    verif_postgis = True
                row=cur.fetchone()
                
            if not verif_asgard:
                QMessageBox.critical(self, "Error", "La connexion "+dbname+ " pointe sur une base \n n'utilisant pas l'extension ASGARD V1.1.0 ou plus")
            if not verif_postgis:
                QMessageBox.critical(self, "Error", "La connexion "+dbname+ " pointe sur une base \n n'utilisant pas l'extension postgis")

            if verif_asgard:
               cur.execute("""
                   SELECT has_schema_privilege( current_user , 'z_asgard', 'USAGE')
                   AND has_table_privilege(current_user, 'z_asgard.asgardmenu_metadata', 'SELECT')
                   """)
               row=cur.fetchone()
               if row[0]:
                  verif_privilege = True
               else:
                  QMessageBox.critical(self, "Error", "L'utilisateur de la connexion "+dbname+ " \n n'a pas les privilèges sur le schéma z_asgard ou la vue z_asgard.asgardmenu_metadata \n Contacter l'administrateur de la base")

                
        return (verif_asgard and verif_postgis and verif_privilege)
            

    def sortby_modelindex(self, rows):
        return sorted(
            rows,
            key=lambda line: '/'.join(
                ['{}'.format(str(elem[0]) + elem[1].lower()) for elem in line[2]]
            ))


    def query_table_metadata(self, current_connection):
        if current_connection in self.dict_alias :    
            alias = self.dict_alias[current_connection]
        else :
            alias = current_connection
        with self.transaction():
            cur = self.connection.cursor()
            if self.layerNameFromComment :
                aRegexp = self.layerRegexp
            else :
                aRegexp = ""
            
            select = dicListSql(self,'optimizedRequestV2')
            #cur.execute("""SET ROLE 'habitat' """)
            cur.execute(select, {
                'connalias' : alias,
                'currentconnexion' : current_connection,
                'regexp' : aRegexp,
                'showconnexion' : self.showConnection,
                'showbloc' : self.showBlocks,
                'showrelationkind' : self.showRelationKind,
                'blocfirst' : self.blockFirst,
                'aexclusionniv1' : self.aexclusionniv1,
                'aexclusionniv2' : self.aexclusionniv2,
                'aexclusionschema' : self.aexclusionschema,
                'aexclusionbloc' :  self.aexclusionbloc                                  
                })
            rows = cur.fetchall()
        return rows

    def build_dock(self, model):
        """
        Construction des menus dans le dockwidget
        """
        menudict = {}        

        if self.regen :
            self.rows=[]

        if self.rows==[] :        
            for current_connection in self.connections_actives :
                connected = self.set_connection(dbname=current_connection)
                if not connected:
                    # don't try to continue 
                    return
                else :
                    self.rows += self.query_table_metadata(current_connection)
                    self.close_connection()   

        model.clear()
                
        if len(self.rows) == 0: 
            QMessageBox.information(self, "Information", "Cette connection n'a accès à aucune ressources \n Menu DOCK vide")
            
        for name, profile, model_index, comment, layername, end_datasource_uri, typegeom, permission, connection, attkey, attfirst in self.sortby_modelindex(self.rows):

            if not attkey : #si pas de clef primaire pour la couche on déclare attfirst comme clef.
                end_datasource_uri= ' key=\''+attfirst+'\''+end_datasource_uri.split('table=')[0]+'checkPrimaryKeyUnicity=\'1\' table='+end_datasource_uri.split('table=')[1]
            
            datasource_uri='vector:postgres:'+layername+':'+self.dict_connections[connection]+end_datasource_uri
            #print(name)
            #print(end_datasource_uri + 'type geom' + str(typegeom))    
            menu = model.invisibleRootItem()

            parent = ''
            for idx, subname in model_index[:-1]:
                parent += '{}-{}/'.format(idx, subname)
                if parent in menudict:
                    # already created entry
                    menu = menudict[parent]
                    continue
                # create menu
                # remplacement d'un code bloc par son alias si le mode bloc est actif                
                iconName = 'menu'
                if self.showBlocks :
                    if len(subname) == 1 and (subname.isalpha() or subname == '_') :
                        iconName = subname
                    if subname in self.dict_bloc_alias :
                        subname = self.dict_bloc_alias[subname]

                item = QStandardItem(subname)
                item.setIcon(QIcon(':/plugins/AsgardMenu/resources/' + iconName +'.svg'))
                uri_struct = QgsMimeDataUtils.Uri(datasource_uri)
                item.setData(uri_struct)
                item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsUserCheckable |
                              Qt.ItemIsEnabled | Qt.ItemIsDropEnabled |
                              Qt.ItemIsEditable)
                item.setWhatsThis("menu")
                menu.appendRow(item)
                menudict[parent] = item
                # set current menu to the new created item
                menu = item

            # add leaf (layer item)

            item = QStandardItem(name)
            uri_struct = QgsMimeDataUtils.Uri(datasource_uri)
            # fix layer name instead of table name
            # usefull when the layer has been renamed in menu
            uri_struct.name = name
            
            # affectation de l'icône
            item.setIcon(QIcon(ICON_MAPPER[self.type_of_layer(typegeom, permission)]))
            
            item.setData(uri_struct)
            # avoid placing dragged layers on it
            item.setDropEnabled(False)
            if uri_struct.providerKey == 'postgres':
                # set tooltip to postgres comment

                item.setToolTip(comment)
            menudict[parent].appendRow(item)

    def save_changes(self, save_to_db=True):
        self.save_session(
            self.activate_dock.isChecked(),
            self.activate_menubar.isChecked(),
            self.showBlocks,
            self.combine,
            self.combineMenuName,
            self.showRelationKind,
            self.layerNameFromComment,
            self.layerRegexp,
            self.splitByBlock,
            self.showConnection,
            self.blockFirst
        )
        self.show_dock(self.activate_dock.isChecked())
        self.show_menus(self.activate_menubar.isChecked())
        return True

    def build_menu(self):
        """
        Construction du menu dans le menu principal de QGIS
        """       
        if self.regen :
            self.rows=[]

        
        if self.rows==[] :        
            for current_connection in self.connections_actives :
                connected = self.set_connection(dbname=current_connection)
                if not connected:
                    # don't try to continue 
                    return
                else :
                    self.rows += self.query_table_metadata(current_connection)
                    self.close_connection() 

        # remove previous menus
        for menu in self.uiparent.menus:
            self.uiparent.iface.mainWindow().menuBar().removeAction(menu.menuAction())
            
        if len(self.rows) == 0: 
            QMessageBox.information(self, "Information", "Cette connection n'a accès à aucune ressources \n Pas de menu")
            
        # item accessor ex: '0-menu/0-submenu/1-item/'
        menudict = {}
        # reference to parent item
        parent = ''
        # reference to qgis main menu bar
        mainMenubar = self.uiparent.iface.mainWindow().menuBar()

        for name, profile, model_index, comment, layername, end_datasource_uri, typegeom, permission, connection, attkey, attfirst in self.sortby_modelindex(self.rows):
                      
            if not attkey : #si pas de clef primaire pour la couche on déclare attfirst comme clef.
                end_datasource_uri= ' key=\''+attfirst+'\''+end_datasource_uri.split('table=')[0]+'checkPrimaryKeyUnicity=\'1\' table='+end_datasource_uri.split('table=')[1]
            
            datasource_uri='vector:postgres:'+layername+':'+self.dict_connections[connection]+end_datasource_uri
                       
            uri_struct = QgsMimeDataUtils.Uri(datasource_uri)
            

            #Eventuel regroupement des menus (cas général)
            if self.combine :            
                model_index.insert(0,[0, self.combineMenuName])


            # root menu            
            parent = '{}-{}/'.format(model_index[0][0], model_index[0][1])

            if parent not in menudict:
                menu = QMenu(self.uiparent.iface.mainWindow())
                self.uiparent.menus.append(menu)
                #remplacement des codes blocs par leur nom complet si necessaire
                if len(model_index[0][1]) == 1 and (model_index[0][1].isalpha() or model_index[0][1] == '_') and model_index[0][1] in self.dict_bloc_alias :
                        model_index[0][1] = self.dict_bloc_alias[model_index[0][1]]
                menu.setObjectName(model_index[0][1])
                menu.setTitle(model_index[0][1])
                mainMenubar.insertMenu(
                    self.uiparent.iface.firstRightStandardMenu().menuAction(),
                    menu)
                menudict[parent] = menu
            else:
                # menu already there
                menu = menudict[parent]

            for idx, subname in model_index[1:-1]:
                # intermediate submenus

                subname=subname.replace("&","&&") #& est interprété comme la désignation du racourçi dans les menus, il faut le doubler.
                #remplacement des codes blocs par leur nom complet si necessaire
                if len(subname) == 1 and (subname.isalpha() or subname == '_') and subname in self.dict_bloc_alias :
                    iconName = subname
                    subname = self.dict_bloc_alias[subname]
                else :
                    iconName = ''
                parent += '{}-{}/'.format(idx, subname)
                if parent not in menudict:
                    submenu = menu.addMenu(subname)
                    submenu.setObjectName(subname)
                    submenu.setTitle(subname)
                    if iconName != '' : submenu.setIcon(QIcon(':/plugins/AsgardMenu/resources/' + iconName +'.svg'))
                    menu = submenu
                    # store it for later use
                    menudict[parent] = menu
                    continue
                # already treated
                menu = menudict[parent]

            layer = QAction(name, self)
            
            # affectation de l'icône
            layer.setIcon(QIcon(ICON_MAPPER[self.type_of_layer(typegeom, permission)]))

            if uri_struct.providerKey == 'postgres':
                # set tooltip to postgres comment
                layer.setStatusTip(comment)
                layer.setToolTip(comment)

            layer.setData(uri_struct.uri)
            layer.setWhatsThis(uri_struct.providerKey)

            layer.triggered.connect(self.layer_handler[uri_struct.layerType])
            menu.addAction(layer)
            
    def type_of_layer(self, typegeom, permission):
        """determine le type de la couche"""
        if typegeom :
            res = [x for x in ['STRING', 'POLYGON', 'POINT'] if x in typegeom]
        else :
            res = ['NoGeometry']
        if not res :
            res = ['INDETERMINE_' + permission]
        else :
            res[0]+= '_'+permission
        return res[0]

    def load_from_index(self, index):
        """Load layers from selected item index"""
        item = self.dock_model.itemFromIndex(self.proxy_model.mapToSource(index))
        if item.whatsThis() == 'menu':
            return
        if item.data().layerType == 'vector':
            layer = QgsVectorLayer(
                item.data().uri,  # uri
                item.text(),  # layer name
                item.data().providerKey  # provider name
            )
        elif item.data().layerType == 'raster':
            layer = QgsRasterLayer(
                item.data().uri,  # uri
                item.text(),  # layer name
                item.data().providerKey  # provider name
            )
        if not layer:
            return
        QgsProject.instance().addMapLayer(layer)

    def load_vector(self):
        action = self.sender()

        layer = QgsVectorLayer(
            action.data(),  # uri
            action.text(),  # layer name
            action.whatsThis()  # provider name
        )
        QgsProject.instance().addMapLayer(layer)

    def load_raster(self):
        action = self.sender()
        layer = QgsRasterLayer(
            action.data(),  # uri
            action.text(),  # layer name
            action.whatsThis()  # provider name
        )
        QgsProject.instance().addMapLayer(layer)

    def set_alias(self):
        """recupère les alias dans self.dict_alias"""
        self.dict_alias = {}
        i=0
        while i < self.table_database.rowCount() :
            if self.table_database.item(i,2) :         
                self.dict_alias[self.table_database.item(i,0).text()]=self.table_database.item(i,2).text()
            i += 1        

    def accept(self):
        self.set_alias()
        self.combineMenuName = self.lineEdit.text()
        self.layerRegexp =  self.lineEdit_2.text()
        if self.save_changes():
            QDialog.reject(self)
            self.close_connection()
        self.rows = [] #réinitialisation de la table retournée par la requête d'interrogation des bases Asgard

    def apply(self):
        self.set_alias()
        self.combineMenuName = self.lineEdit.text()
        self.layerRegexp =  self.lineEdit_2.text()
        if self.save_changes(save_to_db=False):
            #QDialog.reject(self)
            self.close_connection()                       
        self.rows = [] #réinitialisation de la table retournée par la requête d'interrogation des bases Asgard

    def reject(self):
        self.close_connection()
        QDialog.reject(self)

    def close_connection(self):
        """close current pg connection if exists"""
        if getattr(self, 'connection', False):
            if self.connection.closed:
                return
            self.connection.close()

    def save_session(self, dock, menubar, showBlocks, combine, combineMenuName, \
                    showRelationKind, layerNameFromComment, layerRegexp, \
                    splitByBlock, showConnection, blockFirst):
        """save options for next session"""        
        settings = QgsSettings()
        #suppresion des entrées existantes pour les connexions
        settings.beginGroup("/AsgardMenu/connections")
        for connection in settings.childGroups() :
            settings.remove(connection + "/active/")
            settings.remove(connection + "/alias/")
            if settings.value(connection + '/active/', False): #si on trouve quand même qu'une connection est active, c'est que çà vient du qgs_global_settings.ini
                settings.setValue(connection + "/active/", False) #on la met alors à False    
        #sauvegarde des connexions actives
        for connection in self.connections_actives :
            settings.setValue(connection + "/active/", True)
        for connection in self.dict_alias :
            if self.dict_alias[connection] != '' :
                settings.setValue(connection + "/alias/", self.dict_alias[connection])
        settings.endGroup()

        settings.beginGroup("/AsgardMenu/main")
        settings.setValue("dock", dock)
        settings.setValue("menubar", menubar)
        settings.setValue("showBlocks", showBlocks)
        settings.setValue("combine", combine)
        settings.setValue("combineMenuName", combineMenuName)
        settings.setValue("showRelationKind", showRelationKind)
        settings.setValue("layerNameFromComment", layerNameFromComment)
        settings.setValue("layerRegexp", layerRegexp)
        settings.setValue("splitByBlock", splitByBlock)
        settings.setValue("showConnection", showConnection)
        settings.setValue("blockFirst", blockFirst)
        settings.setValue("dialogWidth",self.width())
        settings.setValue("dialogHeight",self.height())
        settings.setValue("column0Width", self.table_database.columnWidth(0))
        settings.setValue("column1Width", self.table_database.columnWidth(1))
        settings.setValue("column2Width", self.table_database.columnWidth(2))
        settings.setValue("dialogX",self.x())
        settings.setValue("dialogY",self.y())
        
        
        settings.endGroup()
        
        #gestion d'un cache en json des données pour le menu
        if self.ficCache:
            with open(self.ficCache,'w') as F :
                json.dump(self.rows, F)

    def restoreDialogBoxSize(self):
        # restauration des dimentions de la boite de dialogue et des colonnes.
        settings = QgsSettings()
        dialogWidth = int(settings.value("AsgardMenu/main/dialogWidth", 565))
        dialogHeight = int(settings.value("AsgardMenu/main/dialogHeight", 606))
        column0Width = int(settings.value("AsgardMenu/main/column0Width", 169))
        column1Width = int(settings.value("AsgardMenu/main/column1Width", 169))
        column2Width = int(settings.value("AsgardMenu/main/column2Width", 192))
        dialogBoxX = int(settings.value("AsgardMenu/main/dialogX", 0))
        dialogBoxY = int(settings.value("AsgardMenu/main/dialogY", 0))
        self.resize(int(dialogWidth), int(dialogHeight))
        self.table_database.setColumnWidth(0,column0Width)
        self.table_database.setColumnWidth(1,column1Width)
        self.table_database.setColumnWidth(2,column2Width)
        if dialogBoxX != 0 or dialogBoxY != 0 :
            self.move(dialogBoxX, dialogBoxY)
        
    
    def restore_session(self):

        #gestion d'un cache en json des données pour le menu
        dossierCache = 'plugin_AsgardMenu'
        nomCache='rows'
        iniFic= QgsSettings().fileName()
        if QFile.exists(iniFic):
            iniDir= os.path.dirname(iniFic)
            if QDir(iniDir +os.sep +dossierCache).exists() or QDir(iniDir).mkdir(dossierCache):
                cheminCache= os.path.abspath(iniDir +os.sep +dossierCache)
                self.ficCache=cheminCache +os.sep +nomCache +'.json'

        
        if QFile.exists(self.ficCache):
            with open(self.ficCache, 'r') as F :
                self.rows=json.load(F)
        else :
            self.rows=[] #si pas de cache réinitialisation de la table retournée par la requête d'interrogation des bases 
        
        #self.rows = [] #réinitialisation de la table retournée par la requête d'interrogation des bases Asgard
        settings = QgsSettings()
        
        #lecture des paramètres pour les connexions
        settings.beginGroup("/AsgardMenu/connections")
        self.connections_actives = []
        self.dict_alias = {}
        self.regen = False
        
        for connection in settings.childGroups() :
            if self.str_to_bool(settings.value(connection + '/active/', False)) : #si la connection est active
                if not self.set_connection(dbname=connection) :
                    QMessageBox.critical(self, "Error", "la connection " + connection + " n'existe plus")
                    self.regen = True
                else :
                    self.connections_actives.append(connection)
            if settings.value(connection + '/alias/', False): #si un alias a été défini
                self.dict_alias[connection]=settings.value(connection + '/alias/')
        settings.endGroup()
        
        #lecture des paramètres pour les exclusions
        aexclusionniv1=settings.value("AsgardMenu/exclusions/aexclusionniv1", [])
        aexclusionniv2=settings.value("AsgardMenu/exclusions/aexclusionniv2", [])
        aexclusionschema=settings.value("AsgardMenu/exclusions/aexclusionschema", [])
        aexclusionbloc=settings.value("AsgardMenu/exclusions/aexclusionbloc", [])
        
        self.aexclusionschema = []
        self.aexclusionniv1 = []
        self.aexclusionniv2 = []
        self.aexclusionbloc = []
                
        if type(aexclusionschema) is not list :
            self.aexclusionschema.append(aexclusionschema)
        else :
            self.aexclusionschema = aexclusionschema
        if type(aexclusionniv1) is not list :
            self.aexclusionniv1.append(aexclusionniv1)
        else:
            self.aexclusionniv1 = aexclusionniv1
        if type(aexclusionniv2) is not list :
            self.aexclusionniv2.append(aexclusionniv2)
        else :
            self.aexclusionniv2 = aexclusionniv2
        if type(aexclusionbloc) is not list :
            self.aexclusionbloc.append(aexclusionbloc)
        else :
            self.aexclusionbloc = aexclusionbloc

        #lecture des paramètres pour les blocs
        #valeurs par défaut :
        self.dict_bloc_alias = {
            'c': 'Consultation',
            'w': 'Donn\xe9\x65s travail',
            's': 'G\xe9ostandards',
            'p': 'Donn\xe9\x65s th\xe9matiques',
            'r': 'R\xe9\x66\xe9rentiels',
            'x': 'Donn\xe9\x65s confidentielles',
            'e': 'Donn\xe9\x65s ext\xe9rieures',
            'z': 'Administration',
            'd': 'Corbeille',
            '_': 'Autres'}
            
        settings.beginGroup("/AsgardMenu/blocs")
       
        for bloc in settings.childKeys() :
            self.dict_bloc_alias[bloc]=settings.value(bloc)
        settings.endGroup()

        #pour des raisons de compatibilité avec les versions antèrieures de asgardMenu 
        # on recherche les clefs dock et menubar directement sous la racine de AsgardMenu

        self.dock = self.str_to_bool(settings.value("AsgardMenu/dock", False))
        self.menubar = self.str_to_bool(settings.value("AsgardMenu/menubar", False))        
        
        
        if not self.dock :
            self.dock = self.str_to_bool(settings.value("AsgardMenu/main/dock", False))
        if not self.menubar :
            self.menubar = self.str_to_bool(settings.value("AsgardMenu/main/menubar", False))
        
        
        self.showBlocks = self.str_to_bool(settings.value("AsgardMenu/main/showBlocks", False))
        self.combine=self.str_to_bool(settings.value("AsgardMenu/main/combine", False))
        self.combineMenuName = settings.value("AsgardMenu/main/combineMenuName", 'Patrimoine')
        self.showRelationKind=self.str_to_bool(settings.value("AsgardMenu/main/showRelationKind", False))
        self.layerNameFromComment = self.str_to_bool(settings.value("AsgardMenu/main/layerNameFromComment", False))
        self.layerRegexp = settings.value("AsgardMenu/main/layerRegexp", '[[]([^]]+)[]]')
        self.splitByBlock = self.str_to_bool(settings.value("AsgardMenu/main/splitByBlock", True))
        self.showConnection = self.str_to_bool(settings.value("AsgardMenu/main/showConnection", True))
        self.blockFirst = self.str_to_bool(settings.value("AsgardMenu/main/blockFirst", True))

        
        if len(self.connections_actives) == 0 :
            return

        self.show_dock(self.dock)
        if self.dock:
            self.uiparent.iface.addDockWidget(Qt.LeftDockWidgetArea, self.dock_widget)
        self.show_menus(self.menubar)
        self.close_connection()                          

    def str_to_bool(self,s) :
        if s == 'false' or s == 'False' or s == False:
            return False
        else : 
            return True
        

class CustomQtTreeView(QTreeView):

    def dragEnterEvent(self, event):
        if not event.mimeData():
            # don't drag menu entry
            return False
        # refuse if it's not a qgis mimetype
        if event.mimeData().hasFormat(QGIS_MIMETYPE):
            event.acceptProposedAction()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Delete:
            model = self.selectionModel().model()
            parents = defaultdict(list)
            for idx in self.selectedIndexes():
                parents[idx.parent()].append(idx)
            for parent, idx_list in parents.items():
                for diff, index in enumerate(idx_list):
                    model.removeRow(index.row() - diff, parent)
        elif event.key() == Qt.Key_Return:
            pass
        else:
            super().keyPressEvent(event)


class DockQtTreeView(CustomQtTreeView):

    def keyPressEvent(self, event):
        """override keyevent to avoid deletion of items in the dock"""
        pass


class MenuTreeModel(QStandardItemModel):

    def dropMimeData(self, mimedata, action, row, column, parentIndex):
        """
        Handles the dropping of an item onto the model.
        De-serializes the data and inserts it into the model.
        """
        # decode data using qgis helpers
        uri_list = QgsMimeDataUtils.decodeUriList(mimedata)
        if not uri_list:
            return False
        # find parent item
        parent_item = self.itemFromIndex(parentIndex)
        if not parent_item:
            return False

        items = []
        for uri in uri_list:
            item = QStandardItem(uri.name)
            item.setData(uri)
            # avoid placing dragged layers on it
            item.setDropEnabled(False)
            if uri.providerKey in ICON_MAPPER:
                item.setIcon(QIcon(ICON_MAPPER[uri.providerKey]))
            items.append(item)

        if row == -1:
            # dropped on a Menu
            # add as a child at the end
            parent_item.appendRows(items)
        else:
            # add items at the separator shown
            parent_item.insertRows(row, items)

        return True

    def mimeData(self, indexes):
        """
        Used to serialize data
        """
        if not indexes:
            return 0
        items = [self.itemFromIndex(idx) for idx in indexes]
        if not items:
            return 0
        if not all(it.data() for it in items):
            return 0
        # reencode items
        mimedata = QgsMimeDataUtils.encodeUriList([item.data() for item in items])
        return mimedata

    def mimeTypes(self):
        return [QGIS_MIMETYPE]

    def supportedDropActions(self):
        return Qt.CopyAction | Qt.MoveAction


class LeafFilterProxyModel(QSortFilterProxyModel):
    """
    Class to override the following behaviour:
        If a parent item doesn't match the filter,
        none of its children will be shown.

    This Model matches items which are descendants
    or ascendants of matching items.
    """

    def filterAcceptsRow(self, row_num, source_parent):
        """Overriding the parent function"""

        # Check if the current row matches
        if self.filter_accepts_row_itself(row_num, source_parent):
            return True

        # Traverse up all the way to root and check if any of them match
        if self.filter_accepts_any_parent(source_parent):
            return True

        # Finally, check if any of the children match
        return self.has_accepted_children(row_num, source_parent)

    def filter_accepts_row_itself(self, row_num, parent):
        return super(LeafFilterProxyModel, self).filterAcceptsRow(row_num, parent)

    def filter_accepts_any_parent(self, parent):
        """
        Traverse to the root node and check if any of the
        ancestors match the filter
        """
        while parent.isValid():
            if self.filter_accepts_row_itself(parent.row(), parent.parent()):
                return True
            parent = parent.parent()
        return False

    def has_accepted_children(self, row_num, parent):
        """
        Starting from the current node as root, traverse all
        the descendants and test if any of the children match
        """
        model = self.sourceModel()
        source_index = model.index(row_num, 0, parent)

        children_count = model.rowCount(source_index)
        for i in range(children_count):
            if self.filterAcceptsRow(i, source_index):
                return True
        return False
