# -*- coding: utf-8 -*-
"""
AsgardMenu -


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from os import path

from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QTimer
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMessageBox, QTableWidgetItem
from qgis.core import QgsApplication


# Initialize Qt resources from file resources.py
from . import resources

# Import the code for the dialog
from .asgard_menu_dialog import AsgardMenuDialog
import os.path


def locale_resource(*filepath):
    """
    filepath should be a list of arguments corresponding to the path remaining
    """
    return path.join(path.abspath(path.dirname(__file__)), *filepath)


class AsgardMenu:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            '{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.plugin_name = self.tr('&Asgard Menu')
        # reference to plugin actions
        self.actions = []
        # used to store active menus
        self.menus = []

        # Create the dialog (after translation) and keep reference
        self.dlg = AsgardMenuDialog(self)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('AsgardMenu', message)

    def initGui(self):
        """Create the plugin entries inside the QGIS GUI."""
        # create the configure entry
        icon = QIcon(':/plugins/AsgardMenu/resources/asgardmenu.svg')
        reload_icon = QIcon(':/plugins/AsgardMenu/resources/reload.png')
        help_icon = QIcon(QgsApplication.iconPath("mActionHelpContents.svg"))
        #self.path = os.path.abspath(os.path.dirname(__file__))
        #self.icons = self.path + os.sep + "resources" + os.sep
        #icon = QIcon(self.icons + 'settings.svg')

        configure = QAction(icon, self.tr('Configuration d\'Asgard menu'), self.iface.mainWindow())
        configure.triggered.connect(self.run_configure)
        configure.setEnabled(True)
        configure.setStatusTip(self.tr("Configure Asgard Menu"))
        configure.setWhatsThis(self.tr("Configure Asgard Menu"))
        
        action_menu_help = QAction(help_icon, self.tr('Aide'), self.iface.mainWindow()) 
        action_menu_help.triggered.connect(lambda: os.startfile("https://snum.scenari-community.org/Asgard/Documentation/#SEC_AsgardMenu"))        
               

        self.actions.append(configure)
        reload = QAction(reload_icon, self.tr('&AsgardMenu actualisation'), self.iface.mainWindow())
        reload.triggered.connect(self.reload_menu)

        self.iface.addPluginToMenu(self.plugin_name, configure)
        self.iface.addPluginToMenu(self.plugin_name, reload)
        self.iface.addPluginToMenu(self.plugin_name, action_menu_help)
        
        #Ajouter une barre d'outils
        self.toolbar = self.iface.addToolBar('AsgardMenu')
        self.toolbar.addAction(configure)
        self.toolbar.addAction(reload)
        self.toolbar.addAction(action_menu_help)
        
        # restore previous session if exists
        self.dlg.restore_session()


    def reload_menu(self) :
        self.dlg.restore_session()

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.plugin_name, action)
        for menu in self.menus:
            menu.deleteLater()
        self.iface.removeDockWidget(self.dlg.dock_widget)
        del self.dlg.dock_widget
        #Supprimer la barre d'outils
        del self.toolbar

    def run_configure(self):
        # show the configure dialog
        self.dlg.show()
        # reload browser content
        #self.dlg.browser.reload()
        self.dlg.restoreDialogBoxSize()
        self.dlg.update_database_list()
        # Run the dialog event loop
        self.dlg.exec_()
