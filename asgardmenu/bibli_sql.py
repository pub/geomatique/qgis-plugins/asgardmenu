# (c) Alain FERRATON 2020 MTE-MCTRCT/SG/SNUM/MSP/DS/GSG Site de Nantes
# créé novembre 2020



#========================================================
#========================================================
def dicListSql(self, mKeySql):
    mdicListSql = {}
                          
    #Requête optimisée PG11 de Leslie 2020.11.04
    mdicListSql['optimizedRequestV2'] = (
                       """ 
WITH asgard_relations AS(
    SELECT
        asgardmenu_metadata.nom_schema,
        asgardmenu_metadata.bloc,
        asgardmenu_metadata.niv1,
        asgardmenu_metadata.niv2,
        pg_class.relname::text,
        pg_class.relkind::text,
        obj_description(pg_class.oid, 'pg_class') AS table_comment,
        asgardmenu_metadata.permission
    FROM pg_catalog.pg_class
        JOIN z_asgard.asgardmenu_metadata
            ON pg_class.relnamespace::regnamespace::text = quote_ident(asgardmenu_metadata.nom_schema)
    WHERE has_table_privilege(pg_class.oid, 'SELECT')
        AND has_schema_privilege(pg_class.relnamespace, 'USAGE')
        AND NOT coalesce(niv1, '') = ANY (%(aexclusionniv1)s)
        AND NOT coalesce(niv2, '') = ANY (%(aexclusionniv2)s)
        AND NOT nom_schema = ANY (%(aexclusionschema)s)
        AND NOT coalesce(bloc, '') = ANY (%(aexclusionbloc)s)
),
asgard_multigeom AS (
    SELECT
        asgard_relations.*,
        count(DISTINCT geometry_columns.f_geometry_column) > 1 AS multigeom,
        array_agg(DISTINCT jsonb_build_array(
            geometry_columns.type,
            geometry_columns.srid,
            geometry_columns.f_geometry_column
            )) AS geom
        FROM asgard_relations LEFT JOIN geometry_columns
            ON geometry_columns.f_table_schema = asgard_relations.nom_schema
                AND geometry_columns.f_table_name = asgard_relations.relname
        GROUP BY asgard_relations.nom_schema, asgard_relations.relname, asgard_relations.bloc,
            asgard_relations.niv1, asgard_relations.niv2, asgard_relations.relkind,
            asgard_relations.table_comment, asgard_relations.permission
    ),
asgard_base AS (
    SELECT
        asgard_multigeom.nom_schema,
        asgard_multigeom.relname::text AS relname,
        asgard_multigeom.bloc,
        asgard_multigeom.niv1,
        asgard_multigeom.niv2,
        asgard_multigeom.relkind,
        asgard_multigeom.table_comment,
        asgard_multigeom.permission,
        asgard_multigeom.multigeom,
        unnest(asgard_multigeom.geom) ->> 0 AS type,
        unnest(asgard_multigeom.geom) ->> 1 AS srid,
        unnest(asgard_multigeom.geom) ->> 2 AS f_geometry_column
        FROM asgard_multigeom
        WHERE asgard_multigeom.relkind = ANY (ARRAY['r', 'v', 'm', 'f', 'p'])
    ),
asgard_alias AS (
    SELECT
        asgard_base.*,
        
        ------ nom de la couche ------
        -- nom de la relation ou alias récupéré dans le commentaire si
        -- le paramétrage le prévoit :
        coalesce(
            CASE WHEN nullif(%(regexp)s, '') IS NOT NULL
                THEN substring(table_comment, %(regexp)s)
            END,
            relname::text
            )
            
        -- si la relation a plusieurs champs de géométrie, nom du champ
        -- considéré pour la couche :
        || CASE
            WHEN multigeom THEN '.'::text || f_geometry_column::text
            ELSE ''::text
        END
        
        -- si doit apparaître, nature de la relation :
        || CASE
            WHEN %(showrelationkind)s AND NOT relkind = 'r' THEN ' (' || upper(relkind::text) || ')'
            ELSE ''::text
        END AS layername
        
        FROM asgard_base
    ),
metadata_completed AS (
    SELECT
        asgard_alias.*,
        array_agg(DISTINCT pg_attribute_pkey.attname::text)
            FILTER(WHERE pg_attribute_pkey.attname IS NOT NULL) AS attkey,
        (array_agg(pg_attribute_first.attname ORDER BY pg_attribute_first.attnum))[1] AS attfirst
        FROM asgard_alias
            LEFT JOIN pg_catalog.pg_index
                ON pg_index.indrelid = (quote_ident(asgard_alias.nom_schema)
                                        || '.' || quote_ident(asgard_alias.relname))::regclass
            LEFT JOIN pg_catalog.pg_attribute AS pg_attribute_pkey
                ON pg_attribute_pkey.attrelid = (quote_ident(asgard_alias.nom_schema)
                                            || '.' || quote_ident(asgard_alias.relname))::regclass
                    AND pg_index.indisprimary
                    AND pg_attribute_pkey.attnum = ANY(pg_index.indkey)
            LEFT JOIN pg_catalog.pg_attribute AS pg_attribute_first
                ON pg_attribute_first.attrelid = (quote_ident(asgard_alias.nom_schema)
                                            || '.' || quote_ident(asgard_alias.relname))::regclass
        WHERE pg_attribute_first.attnum > 0
        GROUP BY asgard_alias.nom_schema, asgard_alias.bloc, asgard_alias.niv1,
            asgard_alias.niv2, asgard_alias.relname, asgard_alias.layername, asgard_alias.relkind,
            asgard_alias.type, asgard_alias.srid, asgard_alias.f_geometry_column,
            asgard_alias.table_comment, asgard_alias.permission, asgard_alias.multigeom
    )
SELECT
    layername,
    'consultation'::text AS profile,
    
    ------ modèle pour les menus ------
    -- connexion si doit apparaître et qu'il n'est
    -- pas spécifié qu'il doit apparaître après les blocs
    CASE
        WHEN %(showconnexion)s AND NOT coalesce(%(blocfirst)s, False)
        THEN jsonb_build_array(jsonb_build_array(0, %(connalias)s))
        ELSE '[]'::jsonb
    END
    
    -- bloc si doit apparaître
    -- '_' pour les schémas sans bloc
    || CASE
        WHEN %(showbloc)s
        THEN jsonb_build_array(jsonb_build_array(
            3 * (coalesce(bloc, '') = 'd')::int -- corbeille
            + 2 * (bloc IS NOT NULL
                AND NOT coalesce(bloc, '') = ANY (ARRAY['c', 'z', 'r', 'e', 'w', 'p', 's', 'x', 'd']))::int
                -- blocs hors nomenclature
            + 1 * (bloc IS NULL)::int, -- schémas sans bloc (pseudo-bloc "Autres")
            coalesce(bloc, '_')
            ))
        ELSE '[]'::jsonb
    END
    
    -- connexion si doit apparaître après les blocs
    || CASE
        WHEN %(showconnexion)s AND %(blocfirst)s
        THEN jsonb_build_array(jsonb_build_array(0, %(connalias)s))
        ELSE '[]'::jsonb
    END
    
    -- niv1 ou nom du schéma
    -- indice à 0 si un niv1 est présent, à 1 sinon
    || jsonb_build_array(jsonb_build_array((niv1 IS NULL)::int, coalesce(niv1, nom_schema)))
    
    -- niv2 ou directement le nom de la couche
    -- indice à 0 si un niv2 est présent, à 1 sinon
    || jsonb_build_array(jsonb_build_array((niv2 IS NULL)::int, coalesce(niv2, layername)))
    
    -- nom de la couche si nécessaire
    || CASE
        WHEN niv2 IS NOT NULL
        THEN jsonb_build_array(jsonb_build_array(0, layername))
        ELSE '[]'::jsonb
    END AS model_index,
    ------

    table_comment,
    relname,

    ------ URI de la couche ------
    CASE
        WHEN srid IS NOT NULL THEN ' srid='::text || srid
        ELSE ''::text
    END
    || CASE
        WHEN type IS NOT NULL THEN ' type='::text || type::text
        ELSE ''::text
    END
    || ' table="'::text || nom_schema::text || '"."'::text || relname::text
    || CASE
        WHEN f_geometry_column IS NOT NULL THEN ('" ('::text || quote_ident(f_geometry_column::text)) || ') sql=::'::text
        ELSE '" sql=:::::NoGeometry'::text
    END AS end_datasource_uri,
    ------

    type AS typegeom,
    permission,
    %(currentconnexion)s as connection,
    attkey,
    attfirst
    FROM metadata_completed                    
                       """
                       )
    return  mdicListSql[mKeySql]


#==================================================
# FIN
#==================================================
