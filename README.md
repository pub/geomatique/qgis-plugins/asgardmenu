# AsgardMenu

## Description

plugin QGIS : Generation automatique de menus pour charger des ressouces (tables, views, etc.) en prenant en compte les privileges des roles de connexion sur les bases de données utilisant l'extension ASGARD dans les bases PostgreSQL.

## Installation
Disponible sur le dépôt [interministériel des plugins QGIS](http://piece-jointe-carto.developpement-durable.gouv.fr/NAT002/QGIS/plugins/plugins.xml)

## Crédits
- Alain FERRATON (DNUM/MSP/DS/GSG)
- Leslie LEMAIRE (DNUM/UNI/DRC)

## License
GNU Affero General Public Licence v3.0 ou plus récent ([AGPL 3 ou plus récent](https://spdx.org/licenses/AGPL-3.0-or-later.html)).


